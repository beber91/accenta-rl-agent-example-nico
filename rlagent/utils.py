import numpy as np


def project_in(value, max_value, min_value):
    return 2 * (value - min_value) / (max_value - min_value) - 1


def project_out(value, max_value, min_value):
    return (value + 1) * (max_value - min_value) / 2 + min_value


def degree_celsius_to_kelvin(degree_celsius):
    return degree_celsius + 273.15


def kelvin_to_degree_celsius(kelvin):
    return kelvin - 273.15


def compute_cop(out_temp):
    DELTA_TEMP_COND = 5.0
    TEMP_BAT_IN_K = degree_celsius_to_kelvin(35.0)
    TEMP_BAT_OUT_K = TEMP_BAT_IN_K - DELTA_TEMP_COND
    ETA = 0.25
    cop_max = 8
    current_weather_kelvin = degree_celsius_to_kelvin(out_temp)

    if current_weather_kelvin >= TEMP_BAT_OUT_K:
        cop_carnot = float("inf")
    else:
        cop_carnot = TEMP_BAT_OUT_K / (TEMP_BAT_OUT_K - current_weather_kelvin)

    cop = cop_carnot * ETA
    cop = min(cop, cop_max)  # COP can't be > than 8

    return cop


def get_time_of_day(x1, x2):
    state_list = [x1, x2]

    # Extract sine and cosine components from the state list
    sine_component = state_list[0]
    cosine_component = state_list[1]

    # Calculate the angle in radians
    angle_radians = np.arctan2(sine_component, cosine_component)
    if angle_radians < 0:
        angle_radians += 2 * np.pi

    # Convert the angle to the corresponding time value in hours
    t = angle_radians / (2.0 * np.pi / (365.0 * 24.0))
    return round(t) % 24


def get_month(x1, x2):
    state_list = [x1, x2]

    # Extract sine and cosine components from the state list
    sine_component = state_list[0]
    cosine_component = state_list[1]

    # Calculate the angle in radians
    angle_radians = np.arctan2(sine_component, cosine_component)
    if angle_radians < 0:
        angle_radians += 2 * np.pi

    # Convert the angle to the corresponding time value in hours
    t = angle_radians / (2.0 * np.pi / (365.0 * 24))
    return round(t) % 720


def clip_actions(action, max_actions):
    return np.clip(action, 0, max_actions)
