from stable_baselines3.common.callbacks import BaseCallback


class RewardCallback(BaseCallback):
    """
    Custom callback for plotting additional values in tensorboard.
    """

    def __init__(self, verbose=0):
        super(RewardCallback, self).__init__(verbose)

    def _on_step(self) -> bool:
        rewards = self.locals["rewards"]
        for i in range(len(rewards)):
            self.logger.record(f"reward/env_{i}", rewards[i])
        # self.logger.record("reward", rewards[0])
        # self.logger.record("reward", self.training_env.get_attr("reward")[0])
        # print("Reward:", self.training_env.get_attr('reward')[0])
        return True
