from joblib import load
import gym
import numpy as np
import pathlib
import pandas as pd
from sklearn.preprocessing import StandardScaler

from rlenv.envs.wall.core import (
    AccentaEnv,
    DEFAULT_FORECAST_GENERATOR,
    DEFAULT_WEATHER_GENERATOR,
    DEFAULT_THERMAL_NEEDS_GENERATOR,
)
from rlagent.utils import get_time_of_day, get_month, compute_cop

HERE = pathlib.Path(__file__).parent


class MyAccentaEnv(AccentaEnv):
    def __init__(
        self,
        n_forecast=6,
        scale_obs=False,
        transform_obs=False,
        transform_act=False,
        infer_a1=False,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.all_hours_forecast = np.load(HERE / "data/all_hours_forecast.npy")
        self.n_forecast = n_forecast
        self.scale_obs = scale_obs
        self.transform_obs = transform_obs
        self.transform_act = transform_act
        if transform_obs:
            obs_space_high = np.array(
                [
                    self.observation_space.high[0],
                    8,
                    self.observation_space.high[1],
                    self.observation_space.high[2],
                    24,
                    12,
                    *self.observation_space.high[0] * np.ones(n_forecast - 1),
                ]
            )
            obs_space_low = np.array(
                [
                    self.observation_space.low[0],
                    0,
                    self.observation_space.low[1],
                    self.observation_space.low[2],
                    0,
                    0,
                    *self.observation_space.low[0] * np.ones(n_forecast - 1),
                ]
            )
            self.observation_space = gym.spaces.Box(
                low=obs_space_low, high=obs_space_high, dtype=np.float32
            )
        if self.scale_obs:
            if self.transform_obs:
                if n_forecast == 24:
                    prev_scaler = load(HERE / "data/scaler_transformed.joblib")
                    self.obs_scaler = StandardScaler()
                    self.obs_scaler.mean_ = np.zeros(29)
                    self.obs_scaler.scale_ = np.ones(29)
                    self.obs_scaler.mean_[:12] = prev_scaler.mean_[:12]
                    self.obs_scaler.scale_[:12] = prev_scaler.scale_[:12]
                    self.obs_scaler.mean_[12:18] = prev_scaler.mean_[6:12]
                    self.obs_scaler.scale_[12:18] = prev_scaler.scale_[6:12]
                    self.obs_scaler.mean_[18:24] = prev_scaler.mean_[6:12]
                    self.obs_scaler.scale_[18:24] = prev_scaler.scale_[6:12]
                    self.obs_scaler.mean_[24:29] = prev_scaler.mean_[6:11]
                    self.obs_scaler.scale_[24:29] = prev_scaler.scale_[6:11]
                else:
                    self.obs_scaler = load(HERE / "data/scaler_transformed.joblib")
            else:
                self.obs_scaler = load(HERE / "data/scaler_not_transformed.joblib")
        if self.transform_act:
            self.act_max = self.action_space.high.copy()[1:]
            self.act_min = self.action_space.low.copy()[1:]
            self.action_space = gym.spaces.Box(
                low=self.act_min, high=self.act_max, dtype=np.float32
            )
        self.infer_a1 = infer_a1

    def step(self, action):
        if self.transform_act:
            action = self.transform_action(action)
        obs, reward, done, info = super().step(action)
        self.power_need = obs[1]
        self.curr_temp = obs[0]
        if self.transform_obs:
            obs = self.transform_observation(obs)
        if self.scale_obs:
            obs = self.scale_observation(obs)
        return obs, reward, done, info

    def reset(self):
        obs = super().reset()
        self.power_need = obs[1]
        self.curr_temp = obs[0]
        if self.transform_obs:
            obs = self.transform_observation(obs)
        if self.scale_obs:
            obs = self.scale_observation(obs)
        return obs

    def transform_observation(self, observation):
        curr_temp = observation[0]
        curr_cop = compute_cop(curr_temp)
        energy_need = observation[1]
        power_storage_level = observation[2]
        t = get_time_of_day(observation[3], observation[4])
        month = get_month(observation[3], observation[4])
        weather_forecast = (
            curr_temp + self.all_hours_forecast[t][1 : self.n_forecast + 1]
        )
        return np.array(
            [
                curr_temp,
                curr_cop,
                energy_need,
                power_storage_level,
                t,
                month,
                *weather_forecast,
            ]
        )

    def scale_observation(self, observation):
        return self.obs_scaler.transform(observation.reshape(1, -1)).reshape(-1)

    def eval(
        cls,
        model,
        scale_obs,
        transform_obs,
        transform_act,
        infer_a1,
        n_forecast,
        weather_generator=None,
        weather_forecast_generator=None,
        thermal_needs_generator=None,
        state_contains_daily_time_indicator: bool = False,
        state_contains_weekly_time_indicator: bool = False,
        state_contains_yearly_time_indicator: bool = True,
        num_episodes: int = 10,
        verbose: bool = False,
        aggregation_method: str = "mean",
    ) -> float:
        """
        Assess the give policy.

        Parameters
        ----------
        model : _type_
            The policy to assess
        weather_generator : _type_, optional
            _description_, by default None
        weather_forecast_generator : _type_, optional
            _description_, by default None
        thermal_needs_generator : _type_, optional
            _description_, by default None
        state_contains_daily_time_indicator : bool, optional
            _description_, by default False
        state_contains_weekly_time_indicator : bool, optional
            _description_, by default False
        state_contains_yearly_time_indicator : bool, optional
            _description_, by default True
        num_episodes : int, optional
            _description_, by default 10
        verbose : bool, optional
            _description_, by default False
        aggregation_method : str, optional
            _description_, by default "mean"

        Returns
        -------
        float
            Aggregated rewards obtained with the given policy

        Raises
        ------
        ValueError
            _description_
        """

        if weather_generator is None:
            weather_generator = DEFAULT_WEATHER_GENERATOR

        if weather_forecast_generator is None:
            weather_forecast_generator = DEFAULT_FORECAST_GENERATOR

        if thermal_needs_generator is None:
            thermal_needs_generator = DEFAULT_THERMAL_NEEDS_GENERATOR

        env = cls(
            n_forecast=n_forecast,
            transform_obs=transform_obs,
            scale_obs=scale_obs,
            transform_act=transform_act,
            infer_a1=infer_a1,
            weather_generator=weather_generator,
            weather_forecast_generator=weather_forecast_generator,
            thermal_needs_generator=thermal_needs_generator,
            record_logs=False,
            state_contains_daily_time_indicator=state_contains_daily_time_indicator,
            state_contains_weekly_time_indicator=state_contains_weekly_time_indicator,
            state_contains_yearly_time_indicator=state_contains_yearly_time_indicator,
        )

        reward_list = []

        for episode_index in range(num_episodes):

            reward_sum = 0

            obs = env.reset()
            done = False

            while not done:
                action, _states = model.predict(obs)
                obs, reward, done, info = env.step(action)
                reward_sum += reward
                # env.render()           # Cannot render on Google Colab

            reward_list.append(reward_sum)

        reward_series = pd.Series(reward_list)

        try:
            aggregated_result = reward_series.aggregate(aggregation_method)
        except AttributeError as e:
            raise ValueError(
                f"Unknown aggregation method {aggregation_method} (c.f. https://pandas.pydata.org/docs/reference/api/pandas.Series.aggregate.html for valid options)"
            )

        if verbose:
            print("Aggregated result:", aggregated_result)
            print("Stats:", reward_series.describe())

        env.close()

        return aggregated_result

    def transform_action(self, action):
        new_action = np.zeros(3)
        new_action[1] = action[0]
        new_action[2] = action[1]
        self.cop = compute_cop(self.curr_temp)
        if self.infer_a1:
            new_action[0] = (self.power_need - new_action[2]) / self.cop
        new_action[0] = max(0, new_action[0])
        return new_action

    def gen_one_episode(
        cls,
        model,
        scale_obs,
        transform_obs,
        transform_act,
        infer_a1,
        n_forecast,
        weather_generator=None,
        weather_forecast_generator=None,
        thermal_needs_generator=None,
        state_contains_daily_time_indicator: bool = False,
        state_contains_weekly_time_indicator: bool = False,
        state_contains_yearly_time_indicator: bool = True,
    ):
        """
        Generate one episode with the given policy.

        Parameters
        ----------
        model : _type_
            _description_
        weather_generator : _type_, optional
            _description_, by default None
        weather_forecast_generator : _type_, optional
            _description_, by default None
        thermal_needs_generator : _type_, optional
            _description_, by default None
        state_contains_daily_time_indicator : bool, optional
            _description_, by default False
        state_contains_weekly_time_indicator : bool, optional
            _description_, by default False
        state_contains_yearly_time_indicator : bool, optional
            _description_, by default True

        Returns
        -------
        Pandas DataFrame
            _description_
        """

        if weather_generator is None:
            weather_generator = DEFAULT_WEATHER_GENERATOR

        if weather_forecast_generator is None:
            weather_forecast_generator = DEFAULT_FORECAST_GENERATOR

        if thermal_needs_generator is None:
            thermal_needs_generator = DEFAULT_THERMAL_NEEDS_GENERATOR

        env = cls(
            n_forecast=n_forecast,
            transform_obs=transform_obs,
            scale_obs=scale_obs,
            transform_act=transform_act,
            infer_a1=infer_a1,
            weather_generator=weather_generator,
            weather_forecast_generator=weather_forecast_generator,
            thermal_needs_generator=thermal_needs_generator,
            record_logs=True,
            state_contains_daily_time_indicator=state_contains_daily_time_indicator,
            state_contains_weekly_time_indicator=state_contains_weekly_time_indicator,
            state_contains_yearly_time_indicator=state_contains_yearly_time_indicator,
        )

        reward_sum = 0
        # env.record_logs = True
        # env_orig.record_logs = True

        obs = env.reset()
        done = False

        while not done:
            action, _states = model.predict(obs)
            obs, reward, done, info = env.step(action)
            reward_sum += reward
            # env.render()           # Cannot render on Google Colab

        reward_sum

        env.close()

        df = env.logs_to_df()

        return df


class SmallAccentaEnv(AccentaEnv):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        obs_space_high = np.zeros(5)
        obs_space_high[:3] = self.observation_space.high[:3]
        obs_space_high[3] = 24
        obs_space_high[4] = 12
        self.observation_space = gym.spaces.Box(
            low=0, high=obs_space_high, dtype=np.float32
        )

    def step(self, action):
        obs, reward, done, info = super().step(action)
        return self.transform_obs(obs), reward, done, info

    def reset(self):
        obs = super().reset()
        return self.transform_obs(obs)

    def transform_obs(self, obs):
        new_obs = np.zeros(5)
        new_obs[:3] = obs[:3]
        t = get_time_of_day(obs[3], obs[4])
        m = get_month(obs[3], obs[4])
        new_obs[3] = t
        new_obs[4] = m
        return new_obs

    def gen_one_episode(
        cls,
        model,
        scale_obs,
        transform_obs,
        transform_act,
        weather_generator=None,
        weather_forecast_generator=None,
        thermal_needs_generator=None,
        state_contains_daily_time_indicator: bool = False,
        state_contains_weekly_time_indicator: bool = False,
        state_contains_yearly_time_indicator: bool = True,
    ):
        """
        Generate one episode with the given policy.

        Parameters
        ----------
        model : _type_
            _description_
        weather_generator : _type_, optional
            _description_, by default None
        weather_forecast_generator : _type_, optional
            _description_, by default None
        thermal_needs_generator : _type_, optional
            _description_, by default None
        state_contains_daily_time_indicator : bool, optional
            _description_, by default False
        state_contains_weekly_time_indicator : bool, optional
            _description_, by default False
        state_contains_yearly_time_indicator : bool, optional
            _description_, by default True

        Returns
        -------
        Pandas DataFrame
            _description_
        """

        if weather_generator is None:
            weather_generator = DEFAULT_WEATHER_GENERATOR

        if weather_forecast_generator is None:
            weather_forecast_generator = DEFAULT_FORECAST_GENERATOR

        if thermal_needs_generator is None:
            thermal_needs_generator = DEFAULT_THERMAL_NEEDS_GENERATOR

        env = cls(
            transform_obs=transform_obs,
            scale_obs=scale_obs,
            transform_act=transform_act,
            weather_generator=weather_generator,
            weather_forecast_generator=weather_forecast_generator,
            thermal_needs_generator=thermal_needs_generator,
            record_logs=True,
            state_contains_daily_time_indicator=state_contains_daily_time_indicator,
            state_contains_weekly_time_indicator=state_contains_weekly_time_indicator,
            state_contains_yearly_time_indicator=state_contains_yearly_time_indicator,
        )

        reward_sum = 0
        # env.record_logs = True
        # env_orig.record_logs = True

        obs = env.reset()
        done = False

        while not done:
            action, _states = model.predict(obs)
            obs, reward, done, info = env.step(action)
            reward_sum += reward
            # env.render()           # Cannot render on Google Colab

        reward_sum

        env.close()

        df = env.logs_to_df()

        return df
