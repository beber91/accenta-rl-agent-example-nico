import pathlib
import numpy as np

from rlagent.utils import compute_cop, clip_actions, get_time_of_day

HERE = pathlib.Path(__file__).parent


class DeterministicBaselineA1:
    def __init__(self, env):
        self.max_actions = env.action_space.high.copy()

    def predict(self, observation):
        a1 = 0
        a2 = 0
        a3 = 0
        power_need = observation[1]
        cop = compute_cop(observation[0])

        a1 = (power_need - a3) / (cop)

        return clip_actions(np.array([a1, a2, a3]), self.max_actions), None


class DeterministicBaselineAll:
    def __init__(self, env):
        self.max_actions = env.action_space.high.copy()
        self.max_storage = 500
        self.all_hours_forecast = np.load(HERE / "data/all_hours_forecast.npy")

    def predict(self, observation):
        a1 = 0.0
        a2 = 0.0
        a3 = 0.0
        power_need = observation[1]
        current_weather = observation[0]
        cop = compute_cop(observation[0])
        current_storage = observation[2]
        t = get_time_of_day(observation[3], observation[4])
        weather_forecast = current_weather + self.all_hours_forecast[t][1:]
        cop_forecast = np.array([compute_cop(x) for x in weather_forecast])

        if cop > np.max(cop_forecast[:6]):
            a2 = (self.max_storage - current_storage) / cop
        if cop < np.min(cop_forecast[:6]) and cop < 0.9 * np.max(cop_forecast):
            a3 = min(current_storage, power_need)

        a2 = min(a2, self.max_actions[1])
        a3 = min(a3, self.max_actions[2])
        a2 = max(a2, 0)
        a3 = max(a3, 0)
        a1 = (power_need - a3) / (cop)
        a1 = min(a1, self.max_actions[0])
        a1 = max(a1, 0)
        # print(a1, a2, a3)
        return np.array([a1, a2, a3]), None
