from stable_baselines3 import PPO
from rlagent.data import ppo_trained_model_example_path

from rlagent.eval_agent import evalAgent
from joblib import load
import numpy as np

import pathlib

from rlenv.envs.wall.core import AccentaEnv

HERE = pathlib.Path(__file__).parent


class DeterministicBaseline:
    def __init__(self):
        self.cop_pred = load(str(HERE / "data/cop_pred.joblib"))
        self.cop_poly = load(str(HERE / "data/cop_poly.joblib"))
        self.max_a1 = 200
        self.max_a2 = 100
        self.max_a3 = 500
        self.max_storage = 500
        self.all_hours_forecast = np.load(str(HERE / "data/all_hours_forecast.npy"))

    def get_time_of_day(self, x):
        return round(np.arcsin(x) / (2 * np.pi / (365.0 * 24.0)) - 2) % 24

    def predict(self, observation):
        a1 = 0
        a2 = 0
        a3 = 0
        power_need = observation[1]
        current_weather = observation[0]
        current_storage = observation[2]
        t = self.get_time_of_day(observation[3])
        weather_forecast = current_weather + self.all_hours_forecast[t][1:]
        tmp = self.cop_poly.transform(current_weather.reshape(1, -1))
        cop = self.cop_pred.predict(tmp)[0]

        tmp = self.cop_poly.transform(weather_forecast.reshape(-1, 1))
        cop_forecast = self.cop_pred.predict(tmp)
        if cop > np.max(cop_forecast[:6]):
            a2 = (self.max_storage - current_storage) / cop
        if cop < np.mean(cop_forecast[:6]) and cop < 0.9 * np.max(cop_forecast):
            a3 = min(current_storage, power_need)

        # if cop>5:
        #     a2 = self.max_storage - current_storage
        #     a3 = 0
        # if cop < 5*0.9:
        #     a3 = min(current_storage, power_need)
        #     a2 = 0

        a1 = (power_need - a3) / (cop)

        a1 = min(a1, self.max_a1)
        a1 = max(a1, 0)
        a2 = min(a2, self.max_storage - current_storage)
        a2 = max(a2, 0)
        a3 = min(a3, self.max_a3)
        a3 = max(a3, 0)
        return np.array([a1, a2, a3]), None


def load_agent():
    """
    This function is mandatory for the ENS Challenge Data evaluation platform to evaluate your agent.

    This function serves as a single, common entry point for all groups to load your trained agent.
    Do not delete it or change its signature (i.e. do not add an argument to it) otherwise your agent will be rejected by the ENS evaluation platform.

    Here an example is provided to load a PPO agent whose weights are loaded from the file returned by the rlagent.data.ppo_trained_model_example_path() function.
    You have to adapt the content of this function to load your **pre-trained** agent.

    Here an example is provided to load a PPO agent whose weights are loaded from the file returned by the rlagent.data.ppo_trained_model_example_path() function.
    You have to adapt the content of this function to load your** pre-trained agent.

    WARNING: this function must return a pre-trained model (e.g. saved in a file included in the git repo, downloaded from the internet, etc.).
    You should not train a model here because there is a timeout on the execution time of this function on the ENS Challenge Data evaluation platform.

    Normally you would load a pre-trained model file, instantiate it and give it as the return value of this function.

    Example code for training an agent is available in the examples directory. You can use them as inspiration to define and train your own agent.

    Returns
    -------
    Gym agent
        The pre-trained model to be evaluated on the ENS Challenge Data platform.
    """
    env = AccentaEnv()

    # Load the agent
    # (in this example the policy_kwargs are automatically loaded from the file returned by `rlagent.data.ppo_trained_model_example_path()`)
    # model = PPO.load(ppo_trained_model_example_path(), env=env, device="cpu")
    # model = DeterministicBaseline()
    ppo_model = PPO.load(HERE / "data/ppo_a2_a3_4e6_24h_forecast.zip", device="cpu")
    model = evalAgent(
        ppo_model,
        infer_a1=True,
        transform_act=True,
        transform_obs=True,
        scale_obs=True,
        n_forecast=24,
    )
    return model
