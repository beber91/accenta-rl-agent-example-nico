from joblib import load
import numpy as np
from pathlib import Path
from sklearn.preprocessing import StandardScaler

from rlagent.utils import compute_cop, get_time_of_day, get_month

HERE = Path(__file__).parent


class evalAgent:
    def __init__(
        self,
        agent,
        infer_a1=True,
        transform_obs=True,
        scale_obs=True,
        transform_act=True,
        n_forecast=6,
    ):
        self.agent = agent
        self.infer_a1 = infer_a1
        self.transform_obs = transform_obs
        self.scale_obs = scale_obs
        self.transform_act = transform_act
        self.n_forecast = n_forecast
        self.all_hours_forecast = np.load(HERE / "data/all_hours_forecast.npy")
        if self.scale_obs:
            if self.transform_obs:
                if n_forecast == 24:
                    prev_scaler = load(HERE / "data/scaler_transformed.joblib")
                    self.obs_scaler = StandardScaler()
                    self.obs_scaler.mean_ = np.zeros(29)
                    self.obs_scaler.scale_ = np.ones(29)
                    self.obs_scaler.mean_[:12] = prev_scaler.mean_[:12]
                    self.obs_scaler.scale_[:12] = prev_scaler.scale_[:12]
                    self.obs_scaler.mean_[12:18] = prev_scaler.mean_[6:12]
                    self.obs_scaler.scale_[12:18] = prev_scaler.scale_[6:12]
                    self.obs_scaler.mean_[18:24] = prev_scaler.mean_[6:12]
                    self.obs_scaler.scale_[18:24] = prev_scaler.scale_[6:12]
                    self.obs_scaler.mean_[24:29] = prev_scaler.mean_[6:11]
                    self.obs_scaler.scale_[24:29] = prev_scaler.scale_[6:11]
                else:
                    self.obs_scaler = load(HERE / "data/scaler_transformed.joblib")
            else:
                self.obs_scaler = load(HERE / "data/scaler_not_transformed.joblib")

    def predict(self, observation):
        if self.transform_obs:
            observation = self.transform_observation(observation)
        if self.scale_obs:
            observation = self.scale_observation(observation)
        action, _ = self.agent.predict(observation, deterministic=True)
        if self.transform_act:
            action = self.transform_action(action)
        return action, observation

    def transform_action(self, action):
        new_action = np.zeros(3)
        new_action[1] = action[0]
        new_action[2] = action[1]
        self.cop = compute_cop(self.curr_temp)
        if self.infer_a1:
            new_action[0] = (self.power_need - new_action[2]) / self.cop
        new_action[0] = max(0, new_action[0])
        return new_action

    def transform_observation(self, observation):
        self.curr_temp = observation[0]
        self.curr_cop = compute_cop(self.curr_temp)
        self.power_need = observation[1]
        self.power_storage_level = observation[2]
        t = get_time_of_day(observation[3], observation[4])
        month = get_month(observation[3], observation[4])
        weather_forecast = (
            self.curr_temp + self.all_hours_forecast[t][1 : self.n_forecast + 1]
        )
        return np.array(
            [
                self.curr_temp,
                self.curr_cop,
                self.power_need,
                self.power_storage_level,
                t,
                month,
                *weather_forecast,
            ]
        )

    def scale_observation(self, observation):
        return self.obs_scaler.transform(observation.reshape(1, -1)).reshape(-1)
